import { Info } from '@/types/TInfo';

export interface IInfoState {
  info: Info;
}
