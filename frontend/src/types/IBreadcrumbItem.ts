export interface IBreadcrumbItem {
  name: string;
  text?: string;
  params?: object;
}
