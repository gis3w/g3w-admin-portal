export interface IGroup {
  id: number;
  name: string;
  description: string;
  title: string;
  srid: number;
  header_logo_link: string | null;
  header_logo_img: string;
}
