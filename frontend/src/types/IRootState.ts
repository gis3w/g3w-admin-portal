export interface IRootState {
  showLoader: boolean;
  errors: string[];
}
