import User from '@/types/TUser';

export interface IUserState {
  me: User | null;
}
