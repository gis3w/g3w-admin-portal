export interface IMacroGroup {
  id: number;
  title: string;
  description: string;
  logo_img: string;
  logo_link: string | null;
}
