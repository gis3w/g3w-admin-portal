export interface IProject {
  id: number;
  title: string;
  description: string;
  thumbnail: string | null;
  edit_url: string | null;
  map_url: string | null;
}
