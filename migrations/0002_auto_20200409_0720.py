# Generated by Django 2.2.9 on 2020-04-09 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='image',
            field=models.ImageField(upload_to='portal_picture', verbose_name='Picture'),
        ),
    ]
